Projet Réalisé par le groupe 7:
- Jérome Chambord
- Vincent Guinaldo
- Justin Lassalle
- Victor Azalbert

Idéalement, les pages sont à consulter en mode mobile.
Pour valider une commande, il faut cliquer sur le lien 'Mon Panier' situé en haut à droite.
